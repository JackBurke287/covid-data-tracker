<style>
  .outside-while{
    border:1px solid black;font-size:18;font-weight:bold;
  }

  .inside-while{
    border:5px solid black;
  }

</style>



<?php 
  require_once 'Setup\config.php';
  $conn = new mysqli($hn, $un, $pw, $db);
  if ($conn->connect_error) die($conn->connect_error);

  $query  = "SELECT * FROM countries ORDER BY Date DESC LIMIT 4";
  $result = $conn->query($query);
  if (!$result) die($conn->error);

  $rows = $result->num_rows;

  for ($row = 0 ; $row < $rows ; ++$row)
  {
    $result->data_seek($row);
	
	
	  echo '<table>';
    echo '<td class=inside-while>Date: '   . $result->fetch_assoc()['Date']   . '<br>';
    $result->data_seek($row);
    echo 'Country: '    . $result->fetch_assoc()['Country']    . '<br>';
    $result->data_seek($row);
    echo 'Confirmed: ' . $result->fetch_assoc()['Confirmed'] . '<br>';
    $result->data_seek($row);
    echo 'Recovered: '     . $result->fetch_assoc()['Recovered']     . '<br>';
    $result->data_seek($row);
    echo 'Deaths: '     . $result->fetch_assoc()['Deaths']     . '<br></td><br>';
	  echo "</table>";
  }


  $result->close();
  $conn->close();
?>
