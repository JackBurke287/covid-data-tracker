<?php

    if(isset($_POST['order'])) {
        $order = $_POST['order'];
        $country = $_POST['country'];
    }
    //Connect to db
    require_once '..\Setup\config.php';
    $conn = new mysqli($hn, $un, $pw, $db);
    if ($conn->connect_error) die($conn->connect_error);

    //Get country data
    $get_country_data = "SELECT * FROM countries WHERE Country = '" . $country . "' ORDER BY " . $order . " DESC";
    $country_data_result = $conn->query($get_country_data);
    if (!$country_data_result) die($conn->error);

    $rows = $country_data_result->num_rows;

    echo '
        <table id="dataTable">
        <b>
            <tr>
                <th>Date</th>
                <th>Confirmed</th>
                <th>Recovered</th>
                <th>Deaths</th>
            </tr>
        </b>';
        for ($row = 0 ; $row < $rows ; ++$row) {
            $country_data_result->data_seek($row);
            echo'<tr class="rows">
                <td>' . $country_data_result->fetch_assoc()['Date'] . '</td>';
                $country_data_result->data_seek($row);
                echo '<td>' . $country_data_result->fetch_assoc()['Confirmed'] . '</td>';
                $country_data_result->data_seek($row);
                echo '<td>' . $country_data_result->fetch_assoc()['Recovered'] . '</td>';
                $country_data_result->data_seek($row);
                echo '<td>' . $country_data_result->fetch_assoc()['Deaths'] . '</td>
            </tr>';
        }
        echo '</table>'
?>