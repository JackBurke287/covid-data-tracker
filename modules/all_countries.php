<?php
//Connect to db
    require_once '..\Setup\config.php';
    $conn = new mysqli($hn, $un, $pw, $db);
    if ($conn->connect_error) die($conn->connect_error);

//Get country list
    $get_countries = "SELECT DISTINCT Country FROM countries ORDER BY Date DESC;";
    $result_countries = $conn->query($get_countries);
    if (!$result_countries) die($conn->error);

    $num_countries = mysqli_num_rows($result_countries);

    $result_countries = $conn->query($get_countries);
    if (!$result_countries) die($conn->error);

    for ($row = 0; $row < $num_countries; ++$row) {
        //Get country data
        $country = $result_countries->fetch_assoc()['Country'];
        $get_country_data = "SELECT * FROM countries WHERE Country = '" . $country . "' ORDER BY Date DESC LIMIT 4";
        $country_data_result = $conn->query($get_country_data);
        $num_rows = mysqli_num_rows($country_data_result);
        if (!$country_data_result) die($conn->error);

        $country_data_result->data_seek(0);

        echo '<a class="card" href ="./country.php?Country=' . $country . '">
            <table class="card-data">
                <tr>
                    <h3>
                        <u>
                            ' . $country . '
                        </u>
                    </h3>
                </tr>
                <tr  class="card-row">
                    <b>
                        <th>Date</th>
                        <th>Confirmed</th>
                        <th>Recovered</th>
                        <th>Deaths</th>
                    </b>
                </tr>';
        for ($card_row = 0 ; $card_row < $num_rows ; ++$card_row) {
            echo '<tr class="card-row">';
            $country_data_result->data_seek($card_row);
            echo '<td>' . $country_data_result->fetch_assoc()['Date'] . '</td>';
            $country_data_result->data_seek($card_row);
            echo '<td>' . $country_data_result->fetch_assoc()['Confirmed'] . '</td>';
            $country_data_result->data_seek($card_row);
            echo '<td>' . $country_data_result->fetch_assoc()['Recovered'] . '</td>';
            $country_data_result->data_seek($card_row);
            echo '<td>' . $country_data_result->fetch_assoc()['Deaths'] . '</td>
                    </tr>';
        }
        echo'</table>
        </a>';
        $country_data_result->close();
    }
    $conn->close();
    exit();
?>
