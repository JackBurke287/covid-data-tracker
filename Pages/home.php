<!DOCTYPE html>

<html lang="en-us">

    <head title="Home">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="..\Assets\style.php">
        <link rel="shortcut icon" type="image/png" href="..\Assets\Favicon.png"/>
    </head>

    <?php
    //Connect to db
    require_once '..\Setup\config.php';
    $conn = new mysqli($hn, $un, $pw, $db);
    if ($conn->connect_error) die($conn->connect_error);

    //Get country list
    $get_countries = "SELECT DISTINCT Country FROM countries ORDER BY Date DESC;";
    $result_countries = $conn->query($get_countries);
    if (!$result_countries) die($conn->error);

    $total_countries = mysqli_num_rows($result_countries);
    $cards_per_page = 20;
    session_start();
    $login = isset($_SESSION['login_user']) ? 'true' : 'false';
    ?>

    <body>

        <!-- Sidebar (hidden by default) -->
        <nav class="sidebar-container" style="display:none;width:15%;" id="Sidebar">
            <?php include_once "../modules/sidebar.php"?>
        </nav>

        <!-- Top menu -->
        <?php include_once "../modules/topMenu.html"?>



        <!-- Content Section -->
        <div class="main-container">
            <div class="big-card-title">
                <h1>
                    <b>
                        All Countries
                    </b>
                </h1>
                <label for="search-all">Search by country name: </label>
                <input type="text" id="search-all" onclick="all_data()" onkeyup="search_countries()" placeholder="eg,Ireland">
            </div>
            <div id="results_box" class="main-container" style="margin-top:10px"></div>

            <!-- Pagination -->
            <div class="pagination-container" id="pagination_controls">
                <?php
                if (isset($_SESSION['login_user'])) {
                    $num_pages = intval(192 / 20);
                    if ($num_pages > 1) {
                        for ($page = 0; $page < ($num_pages + 1); ++$page) {
                            echo '<a onclick= request_page(' . $page . ') class="page-box">' . ($page + 1) . '</a>';
                        }
                    }
                }
                ?>
            </div>
        </div>

        <!-- End page content -->
        <script src="../assets/functions.js"></script>

        <script>
            //get data for graph and pagenatable table
            function request_page(page_number){
                const results_box = document.getElementById("results_box");
                let cards_per_page = <?php echo $cards_per_page;?>;
                let total_countries = <?php echo $total_countries;?>;
                if (<?php echo $login?>){
                    results_box.innerHTML = "<img src='../assets/loading.gif'>";
                    const request = new XMLHttpRequest();
                    request.open("POST", "../modules/pagination_parser.php", true);
                    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.onreadystatechange = function () {
                        if (request.readyState == 4 && request.status == 200) {
                            results_box.innerHTML = request.responseText;
                        }
                    }
                    request.send("cards_per_page="+cards_per_page+"&page_number="+page_number+"&total_countries="+total_countries);

                }
                else{
                    results_box.innerHTML = '<div class="big-card-main"><h3>Login To See Data</h3></div>';
                }
            }

            //get all data for dynamic search
            function all_data(){
                const results_box = document.getElementById("results_box");
                if (<?php echo $login?>){
                    results_box.innerHTML =
                        "<div class='loader'>" +
                        "<img src='../assets/loading.gif'>" +
                        "<h3>Please wait while we load all data...</h3>" +
                        "</div>";
                    const request = new XMLHttpRequest();
                    request.open("POST", "../modules/all_countries.php", true);
                    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.send();
                    request.onreadystatechange = function () {
                        if (request.readyState == 4 && request.status == 200) {
                            results_box.innerHTML = request.responseText;
                            search_countries();
                        }
                    }
                }
                else{
                    results_box.innerHTML = '<div class="big-card-main"><h3>Login To See Data</h3></div>';
                }
            }

            //dynamic search
            function search_countries(){
                let cards = document.getElementsByClassName("card")
                let input = document.getElementById("search-all");
                let filter = input.value.toUpperCase();
                // Loop through all underlined items, and hide those who don't match the search query
                for (let i = 0; i < cards.length; i++) {
                    let country = cards[i].getElementsByTagName("u")[0];
                    let textValue = country.textContent || country.innerText;
                    if (textValue.toUpperCase().indexOf(filter) > -1){
                        cards[i].style.display = "";
                    }
                    else{
                        cards[i].style.display = "none";
                    }
                }
            }
        </script>
        <script>
            request_page(0);
        </script>
    </body>
</html>
