<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="..\Assets\style.php">
        <link rel="shortcut icon" type="image/png" href="..\Assets\Favicon.png"/>
    </head>

    <?php
    require_once '..\Setup\config.php';
    $conn = new mysqli($hn, $un, $pw, $db);
    if ($conn->connect_error) die($conn->connect_error);
    $data_check = false;
    session_start();

    if($_SERVER["REQUEST_METHOD"] == "POST") {
        // username and password sent from form

        $country = mysqli_real_escape_string($conn,$_POST['country']);
        $date = mysqli_real_escape_string($conn,$_POST['date']);
        $confirmed = mysqli_real_escape_string($conn,$_POST['confirmed']);
        $recovered = mysqli_real_escape_string($conn,$_POST['recovered']);
        $deaths = mysqli_real_escape_string($conn,$_POST['deaths']);

        if (preg_match("/[\s\S]/", $country) === 1 and preg_match("/[\s\S]/", $date) === 1){
            $data_check = true;
        } else{
            $error = "The country and date fields must not be empty";
        }

        if ($data_check == true){
            //check if the date for this country exists. if it does throw an error
            $sqlcheck = "SELECT Date FROM countries WHERE Date = '$date' AND Country = '$country'";
            $resultcheck = mysqli_query($conn,$sqlcheck);
            $rowcheck = mysqli_fetch_array($resultcheck,MYSQLI_ASSOC);
            $countcheck = mysqli_num_rows($resultcheck);

            if($countcheck == 0) {
                $sqladd = "INSERT INTO countries(Date, Country, Confirmed, Recovered, Deaths) VALUES ('$date', '$country', '$confirmed', '$recovered', '$deaths')";
                $resultadd = mysqli_query($conn,$sqladd);
                $success = "Successfully addded data for " . $country;
            }else if ($countcheck > 0) {
                $error = "An entry already exists with this date, try a different date";
            }
            else{
                $error = "Something went wrong";
            }
        }
    }
    ?>

    <body>

    <!-- Sidebar (hidden by default) -->
    <nav class="sidebar-container" style="display:none;width:15%;" id="Sidebar">
        <?php include_once "../modules/sidebar.php"?>
    </nav>

    <!-- Top menu -->
    <?php include_once "../modules/topMenu.html"?>

    <!-- Content Section -->
    <div class="main-container">
        <div class="big-card-title">
            <h1>
                <b>
                    Add Data
                </b>
            </h1>
        </div>

        <!-- Big Card -->
        <div class="big-card-main">
            <div class="big-card-column">
                <?php
                if(isset($error)){
                    echo '<div class="error-message">' . $error . '</div>';
                    unset($error);
                }
                elseif (isset($success)){
                    echo '<div class="success-message">' . $success . '</div>';
                    unset($success);
                }
                ?>
                <h3>
                    <b>
                        <form action = "" method = "post">
                            <label>Country  : </label><input class="form-field" type = "text" name = "country" placeholder="eg,Ireland" class = "box"/><br/><br/>
                            <label>Date  : </label><input class="form-field" type = "date" name = "date" placeholder="yyyy-mm-dd" value="0" class = "box" /><br/><br/>
                            <label>Confirmed  : </label><input class="form-field" type = "text" name = "confirmed" placeholder="0" value="0" class = "box"/><br/><br/>
                            <label>Recovered  : </label><input class="form-field" type = "text" name = "recovered" placeholder="0" value="0" class = "box"/><br/><br/>
                            <label>Deaths  : </label><input class="form-field" type = "text" name = "deaths" placeholder="0" value="0" class = "box"/><br/><br/>
                            <input class="form-button" type = "submit" value = " Add Data "/><br/><br/>
                        </form>
                    </b>
                </h3>
            </div>
        </div>
    </div>

    <script src="../assets/functions.js"></script>

    </body>

</html>

