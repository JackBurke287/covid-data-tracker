<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="..\Assets\style.php">
        <link rel="shortcut icon" type="image/png" href="..\Assets\Favicon.png"/>
    </head>

    <?php
    require_once '..\Setup\config.php';
    $conn = new mysqli($hn, $un, $pw, $db);
    if ($conn->connect_error) die($conn->connect_error);
    session_start();

    if($_SERVER["REQUEST_METHOD"] == "POST") {
        // username and password sent from form

        $myusername = mysqli_real_escape_string($conn,$_POST['username']);
        $mypassword = mysqli_real_escape_string($conn,$_POST['password']);

        $pass_sql = "SELECT Password FROM users WHERE UserName = '$myusername'";
        $pass_result = mysqli_query($conn,$pass_sql);
        $pass_count = mysqli_num_rows($pass_result);
        $pass_result->data_seek(0);
        if($pass_count ==1){
            $hash = $pass_result->fetch_assoc()['Password'];
            //password hash validation
            $hash_bool = password_verify ($mypassword ,$hash ) ? 'TRUE' : 'FALSE';
        }
        else{
            $hash_bool = "FALSE";
        }

        $user_sql = "SELECT PersonID, Admin FROM users WHERE UserName = '$myusername' AND " . $hash_bool;
        $user_result = mysqli_query($conn,$user_sql);

        $user_count = mysqli_num_rows($user_result);
        $user_result->data_seek(0);

        // login or error handling based on details entered
        if($user_count == 1) {
            $admin = $user_result->fetch_assoc()['Admin'];
            $_SESSION['login_user'] = $myusername;
            $_SESSION['admin'] = $admin;
            //go back home after login
            header("location: home.php");
        }
        else if ($pass_count == 1) {
            $error = "Your Password is invalid";
        }
        else{
            $error = "Your Username and Password is invalid";
        }
    }
    ?>

    <body>

        <!-- Sidebar (hidden by default) -->
        <nav class="sidebar-container" style="display:none;width:15%;" id="Sidebar">
            <?php include_once "../modules/sidebar.php"?>
        </nav>

        <!-- Top menu -->
        <?php include_once "../modules/topMenu.html"?>

        <!-- Content Section -->
        <div class="main-container">

            <div class="big-card-title">
                <h1>
                    <b>
                        Login
                    </b>
                </h1>
            </div>

            <!-- Big Card -->
            <div class="big-card-main">
                <div class="big-card-column">
                    <?php
                    if(isset($error)){
                        echo '<div class="error-message">' . $error . '</div>';
                        unset($error);
                    }
                    ?>
                    <h3>
                        <b>
                            <form action = "" method = "post">
                                <label>UserName  : </label><input class="form-field" type = "text" name = "username" placeholder="Your username.." class = "box"/><br/><br/>
                                <label>Password  : </label><input class="form-field" type = "password" name = "password" placeholder="Your password.." class = "box" /><br/><br/>
                                <input class="form-button" type = "submit" value = " Submit "/>
                            </form>
                        </b>
                    </h3>

                    <div class="sign-up-container">
                        dont have an account? <a class="form-button" href ="./signup.php">Signup</a>
                    </div>
                </div>
            </div>
        </div>

        <script src="../assets/functions.js"></script>

        <script>
            w3IncludeHTML();
        </script>
    </body>
</html>

