<!DOCTYPE html>
<html lang="en-us">

    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="..\Assets\style.php">
        <link rel="shortcut icon" type="image/png" href="..\Assets\Favicon.png"/>
    </head>


    <body>

        <!-- Sidebar (hidden by default) -->
        <nav class="sidebar-container" style="display:none;width:15%;" id="Sidebar">
            <?php
            session_start();
            include_once "../modules/sidebar.php"?>
        </nav>

        <!-- Top menu -->
        <?php include_once "../modules/topMenu.html"?>

          <!-- Content Section -->
        <div class="main-container">
            <div class="big-card-title">
                <h1>
                    <b>
                        <?php
                        echo $_GET['Country'];
                        ?>
                    </b>
                </h1>
                <label for="order_dropdown">Order By: </label>
                <select name = "navyOp" id="order_dropdown" onChange="order_by(value)">
                    <option value = "Date">Date</option>
                    <option value = "Confirmed">Confirmed</option>
                    <option value = "Recovered">Recovered</option>
                    <option value = "Deaths">Deaths</option>
                </select>
                <div class="graph big-card-main">
                    <div id="curve_chart"></div>
                </div>
            </div>

            <!-- Big Cards -->
            <div class="big-card-main" id="main_card">
            </div>
        </div>

        <script src="../assets/functions.js"></script>
        <script>
            function order_by(order){
                let country = "<?php echo $_GET['Country'];?>";
                var results_box = document.getElementById("main_card");
                results_box.innerHTML = "<img src='../assets/loading.gif'>";
                var request = new XMLHttpRequest();
                request.open("POST", "../modules/country_data_parser.php", true);
                request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                request.onreadystatechange = function () {
                    if (request.readyState == 4 && request.status == 200) {
                        results_box.innerHTML = request.responseText;
                    }
                }
                request.send("order="+order+"&country="+country);
            }
            order_by("Date")
            </script>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

        <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            async function drawChart() {

                const delay = ms => new Promise(res => setTimeout(res, ms));
                await delay(100);

                let array = [];
                let rows = document.getElementsByTagName("tr");
                for (let i = 1; i < rows.length; i++) {
                    let cells = rows.item(i).cells;
                    array.push([cells[0].textContent, parseInt(cells[1].textContent), parseInt(cells[2].textContent), parseInt(cells[3].textContent)]);
                }
                array.push(["Date", "Confirmed", "Recovered", "Deaths"]);
                array = array.reverse();

                var data = google.visualization.arrayToDataTable(array);

                var options = {
                    title: 'Cases over time',
                    curveType: 'function',
                    legend: {position: 'bottom'},
                    'is3D': true,
                    'width': 960,
                    'height': 400,
                    backgroundColor: '#dcdde3'
                };

                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

                chart.draw(data, options);
            }
        </script>

</body>
</html>
