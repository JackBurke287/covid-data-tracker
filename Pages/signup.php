<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="..\Assets\style.php">
        <link rel="shortcut icon" type="image/png" href="..\Assets\Favicon.png"/>
        <title>Signup</title>
    </head>

    <?php
    require_once '..\Setup\config.php';
    $conn = new mysqli($hn, $un, $pw, $db);
    if ($conn->connect_error) die($conn->connect_error);
    $user_check = false;
    $pass_check = false;
    session_start();

    if($_SERVER["REQUEST_METHOD"] == "POST") {
        // username and password sent from form

        $myusername = mysqli_real_escape_string($conn,$_POST['username']);
        $mypassword = mysqli_real_escape_string($conn,$_POST['password']);
        $admin = mysqli_real_escape_string($conn,$_POST['admin']);

        if(preg_match("^$^", $myusername) === 0){
            $user_check = true;
        }
        else{
            $error = "Username cannot be empty.";
        }
        if (preg_match("^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*^", $mypassword) == 1){
            $pass_check = true;
        }
        else{
            $error = "Your password must be at least 8 characters long,<br> contain 1 uppercase letter, 1 lowercase letter and 1 number.";
        }

        if ($user_check == true and $pass_check == true){
            //check it user exists. if it does throw an error
            $sqlcheck = "SELECT PersonID FROM users WHERE UserName = '$myusername'";
            $resultcheck = mysqli_query($conn,$sqlcheck);
            $rowcheck = mysqli_fetch_array($resultcheck,MYSQLI_ASSOC);
            $countcheck = mysqli_num_rows($resultcheck);

            if($countcheck == 0) {
                //hash password
                $hashed_password = password_hash($mypassword, PASSWORD_DEFAULT);

                $sqladd = "INSERT INTO users(UserName, Password, Admin) VALUES ('$myusername', '$hashed_password', '$admin')";
                $resultadd = mysqli_query($conn,$sqladd);
                $sql = "SELECT PersonID, Admin FROM users WHERE UserName = '$myusername' and Password = '$hashed_password'";
                $result = mysqli_query($conn,$sql);

                $count = mysqli_num_rows($result);

                // If result matched $myusername and $mypassword, table row must be 1 row

                if($count == 1) {
                    $admin = $result->fetch_assoc()['Admin'];
                    $_SESSION['login_user'] = $myusername;
                    $_SESSION['admin'] = $admin;
                    header("location: home.php");
                }
                header("location: home.php");
            }
            else {
                $error = "A user already exists with this username, try a different username";
            }
        }
    }
    ?>

    <body>

    <!-- Sidebar (hidden by default) -->
    <nav class="sidebar-container" style="display:none;width:15%;" id="Sidebar">
        <?php include_once "../modules/sidebar.php"?>
    </nav>

    <!-- Top menu -->
    <?php include_once "../modules/topMenu.html"?>

    <!-- Content Section -->
    <div class="main-container">
        <div class="big-card-title">
            <h1>
                <b>
                    Signup
                </b>
            </h1>
        </div>

        <!-- Big Card -->
        <div class="big-card-main">
            <div class="big-card-column">
                <?php
                if(isset($error)){
                    echo '<div class="error-message">' . $error . '</div>';
                    unset($error);
                }
                ?>
                <h3>
                    <b>
                        <form action = "" method = "post">
                            <label>UserName  : </label><input class="form-field" type = "text" name = "username" placeholder="Your username.." class = "box"/><br/><br/>
                            <label>Password  : </label><input class="form-field" type = "password" name = "password" placeholder="Your password.." class = "box" /><br/><br/>
                            <input type="hidden" name="admin" value="0"/>
                            <label for="Admin"> Is this user an Admin?</label>
                            <input type="checkbox" name="admin" value="1"/><br><br>
                            <input class="form-button" type = "submit" value = " Signup "/><br/><br/>
                        </form>
                    </b>
                </h3>
            </div>
        </div>
    </div>


    <script src="../assets/functions.js"></script>
    <script>
        w3IncludeHTML();
    </script>


    </body>

</html>

