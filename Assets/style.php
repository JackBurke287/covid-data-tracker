<?php header("content-type:text/css; charset:UTF-8");?>

:root{
    --prime-color: #172e42;
    --secondary-color: #dcdde3;
    --secondary-hover-color: #c5c6d1;
    --primary-hover-color: #1e3d59;
}

body, h1, h2, h3, h4, h5, h6, a, p, input[type=submit] {
    font-family: 'OCR A Std', monospace;
    text-decoration: none;
    color: var(--prime-color-color);
}

h1{
    margin: 5px 5px 5px 5px;
}

body{
    background-color: var(--prime-color);
}

.main-container {
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    margin-top: 65px;
}

.big-card-title {
    text-align: center;
    width: 960px;
    color: var(--secondary-color);
}

.big-card-main{
    display: flex;
    justify-content: center;
    text-align: center;
    width: 100%;
    max-width:960px;
    background-color: var(--secondary-color);
    border-radius: 5px;
}

.big-card-column{
    justify-content: center;
    text-align: center;
}

table {
    width: 80%;
    text-align: center;
}

.card {
    min-width: 400px;
    margin: 0.5rem;
    background-color: var(--secondary-color);
    border-radius: 5px;
    transition: 0.20s;
}

.card-data {
    width: 100%;
    padding:  1em;
}

.card-row {
    padding:10px;
    height:30px;
}

.card:hover{
    transform: scale(1.02);
    background-color: var(--secondary-hover-color);
}


@media(max-width: 1px){
    .card{
        background-color: blue;
    }
}

.card h3 {
    text-align:center;
}

.card-column-container{
    display: flex;
    flex-direction: row;
}

.card-column{
    padding: 1rem;
    text-align: center;
}

.nav-bar{
    z-index: 1;
    top: 0;
    width: 100%;
    max-width:960px;
    position: fixed;
    font-size: 40px;
    background-color: var(--secondary-color);
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
    border-style: solid;
    border-width: 1px;
    height:60px;
}

.nav-bar-container{
    display: flex;
    flex-direction: row;
    justify-content: center;
}

.menu-button{
    position: absolute;
    left:0;
    margin: 0px 0px 5px 5px;
}

.sidebar-container{
    height:100%;
    background-color: var(--prime-color);
    position:fixed!important;
    z-index:2;
    overflow:auto;
    overflow:auto;
}

.sidebar-button{
    display: flex;
    justify-content: left;
    flex:direction:column;
    padding: 1rem;
    font-size: 20px;
    color: var(--secondary-color);
}

.sidebar-button:hover{
    background-color: var(--primary-hover-color);
}

.pagination-container{
    width:100%;
    display: flex;
    justify-content: center;
    flex:direction:column;
    padding: 1rem;
}

.page-box{
    background-color: var(--secondary-color);
    margin: 0.5rem;
    min-width: 30px;
    text-align: center;
    border-radius: 5px;
    transition: 0.20s;
    }

.page-box-current{
    transform: scale(1.2);
    background-color: var(--secondary-hover-color);
}

.page-box:hover{
    transform: scale(1.2);
    background-color: var(--secondary-hover-color);
}

.footer-container{
    display: flex;
    justify-content: center;
}

.graph-container{
    text-align:center;
}

.graph-container{
    display: flex;
    justify-content: center;
    margin: 65px 0px 5px 0px;
}

.form-field{
    border-style: solid;
    margin: 0.5rem;
    background-color: white;
    border-radius: 2px;
    border-width: 1px;
    border-color:var(--primary-color);
}

.form-button{
    border-style: none;
    margin: 0.5rem;
    background-color: var(--secondary-hover-color);
    border-radius: 2px;
    padding: 0.5rem;
}

.form-button:hover{
    background-color: var(--prime-color);
    color: var(--secondary-color);
}

.error-message{
    color: red;
    padding: 1rem;
}

.success-message{
color: green;
padding: 1rem;
}

.sign-up-container{
    padding: 1rem;
}

.loader{
    justify-content: center;
    text-align: center;
    flex-direction: column;
    flex-wrap: wrap;
    color: var(--secondary-hover-color);
}

.graph{
display: flex;
justify-content: center;
max-width: 960px;
padding: 1rem 0rem 1rem 0rem;
margin: 1rem 0rem 1rem 0rem;
}






