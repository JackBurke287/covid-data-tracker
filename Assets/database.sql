DROP DATABASE IF EXISTS coviddata;

CREATE DATABASE coviddata;

DROP TABLE IF EXISTS coviddata.users;

CREATE TABLE coviddata.users
(
    PersonID int AUTO_INCREMENT
        PRIMARY KEY,
    UserName varchar(255) NULL,
    Password varchar(255) NULL,
    Admin    tinyint(1)   NULL
);

