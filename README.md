# Covid Data #

This README Documents how to set up my Covid Data project.

### What is this repository for? ###

* A data tracking tool for Covid-19 statistics by country.
* v1.0

### How do I get set up? ###

1. Add the project CovidApp to the htdocs folder in xampp.
2. Open the file `Assets/database.sql` in Datagrip (or any SQL IDE that can do .csv imports).
3. Execute the file on your Xampp server.
4. Import the CSV `countries.csv` into the coviddata database (see "How to import CSV files in Datagrip?" for steps).
5. Configure the file `Setup/config.php` to work with your database.
6. Go to : http://localhost./home.php
7. you should be able to use the website.

### How to import CSV files in Datagrip? ###

1. Right-click on the database that you want to import the data to.
2. Click "Import Data From File...".
3. Find and select the `countries.csv` file.
4. Make sure the "First row is header" option is selected on the left column.
5. Make sure the Date data type is `DATE`.
6. Click import on the bottom right.

### Owner ###

* Jack Burke - D19125736